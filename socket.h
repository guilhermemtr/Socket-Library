#ifndef __SOCK__
#define __SOCK__

#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define _SOCK_MODE AF_INET //ip v4. Use AF_INET6 for ip v6
#define _SOCK_MAX_CON (1 << 5)

typedef struct {
  struct hostent *srv;
  struct sockaddr_in srv_addr;
  int port;
} sock_clt_t;

typedef struct {
  struct sockaddr_in srv;
  int port;
  int l_fd;
} sock_srv_t;

typedef struct {
  struct sockaddr_in srv, clt;
  int fd;
} sock_con_t;

sock_srv_t *
create_srv_sock (int port);

void
destroy_srv_sock(sock_srv_t * s_srv);

sock_con_t *
accept_con (sock_srv_t * s_srv);

sock_con_t *
clt_connect (sock_clt_t * s_clt);

void
destroy_con (sock_con_t * con);

sock_clt_t *
create_clt_sock (char * addr, int port);

void
destroy_clt_sock (sock_clt_t * s_clt);

#endif
