#!/bin/bash
CC = gcc
RM = rm -f

SRC_PATH =
INC_PATH = 

CFLAGS = -fPIC -march=native -Wall $(MODE) #-I$(INC_PATH)
SFLAGS = -shared
LDFLAGS =

SOURCES = socket.c
OBJECTS = $(SOURCES:.c=.o)

SOLIB = libsock.so

MODE = $(RELEASE)

RELEASE = -O2
DEBUG = -g -O0

all: $(SOURCES) $(SOLIB)
	$(CC) $(CFLAGS) $(OBJECTS) $(SFLAGS) -o $(SOLIB) $(LDFLAGS)

$(SOLIB):$(OBJECTS)

.o:
	$(CC) $(CFLAGS) $< $@ $(LDFLAGS)

clean:
	$(RM) $(OBJECTS) $(SOLIB)
