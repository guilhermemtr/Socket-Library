#include <stdio.h>
#include "socket.h"

int
main(int argc, char ** argv)
{
  sock_srv_t * srv = create_srv_sock(9001);
  sock_con_t * con = accept_con(srv);
  write(con->fd, "HELLO", 5);
  destroy_con(con);
  destroy_srv_sock(srv);
  return 0;
}
