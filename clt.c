#include <stdio.h>
#include "socket.h"


int
main(int argc, char ** argv)
{
  char * srv = "localhost";
  int port = 9001;
  sock_clt_t * clt = create_clt_sock(srv,port);
  sock_con_t * con = clt_connect(clt);
  char buf[256];
  bzero(buf,256);
  int n = read(con->fd,buf,255);
  printf("%s\n",buf);
  return 0;
}
