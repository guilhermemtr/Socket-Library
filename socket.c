#include "socket.h"

#ifdef __SOCK__

sock_srv_t *
create_srv_sock (int port)
{
  sock_srv_t * new_sock = (sock_srv_t *) malloc(sizeof(sock_srv_t));
  new_sock->l_fd = socket(_SOCK_MODE, SOCK_STREAM, 0);
  if (new_sock->l_fd < 0) {
    free(new_sock);
    return NULL;
  }
  bzero((char *) &(new_sock->srv), sizeof(struct sockaddr_in));
  new_sock->srv.sin_family = _SOCK_MODE;
  new_sock->srv.sin_addr.s_addr = INADDR_ANY;
  new_sock->srv.sin_port = htons(port);
  if (bind(new_sock->l_fd, (struct sockaddr *) &(new_sock->srv), sizeof(struct sockaddr_in)) < 0) {
    free(new_sock);
    return NULL;
  }

  listen(new_sock->l_fd,_SOCK_MAX_CON);

  return new_sock;
}

void
destroy_srv_sock(sock_srv_t * s_srv)
{
  close(s_srv->l_fd);
  free(s_srv);
}

sock_con_t *
accept_con (sock_srv_t * s_srv)
{
  sock_con_t * con = (sock_con_t *) malloc(sizeof(sock_con_t));
  socklen_t clilen = sizeof(struct sockaddr_in);
  con->fd = accept(s_srv->l_fd, (struct sockaddr *) &(con->clt), &clilen);
  con->srv = s_srv->srv;
  if (con->fd < 0) {
    free(con);
    return NULL;
  }
  return con;
}

void
destroy_con (sock_con_t * con)
{
  close(con->fd);
  free(con);
}

sock_con_t *
clt_connect (sock_clt_t * s_clt)
{
  sock_con_t * con = (sock_con_t *) malloc(sizeof(sock_con_t));
  con->fd = socket(AF_INET, SOCK_STREAM, 0);
  if (con->fd < 0) {
    free(con);
    return NULL;
  }

  if (connect(con->fd,(const struct sockaddr *) &(s_clt->srv_addr),sizeof(struct sockaddr_in)) < 0) {
    free(con);
    return NULL;
  }

  return con;
}

sock_clt_t *
create_clt_sock (char * addr, int port)
{
  sock_clt_t * new_sock = (sock_clt_t *) malloc(sizeof(sock_clt_t));
  new_sock->srv = gethostbyname(addr);
  if (new_sock->srv == NULL) {
    free(new_sock);
    return NULL;
  }
  bzero((char *) &(new_sock->srv_addr), sizeof(struct sockaddr_in));
  new_sock->srv_addr.sin_family = AF_INET;
  bcopy((char *)new_sock->srv->h_addr, (char *)&(new_sock->srv_addr.sin_addr.s_addr), new_sock->srv->h_length);
  new_sock->srv_addr.sin_port = htons(port);

  return new_sock;
}

void
destroy_clt_sock (sock_clt_t * s_clt)
{
  free(s_clt);
}

#endif
